export const environment = {
  production: true,
  movieApiUrl: 'https://angularcoursebackend.azurewebsites.net/api/movies',
  tvShowApiUrl: 'https://angularcoursebackend.azurewebsites.net/api/tvshows'
};
