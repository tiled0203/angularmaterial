import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from "rxjs/operators";
import {LoginService} from "./login.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private loginService: LoginService, private snackBar: MatSnackBar) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req.clone({headers: req.headers.set('Authorization', this.loginService.user)});

    const today = new Date();
    return next.handle(authReq).pipe(tap(response => {
        if (response instanceof HttpResponse) {
          const elapsed = new Date().getMilliseconds() - today.getMilliseconds();
          console.log("Elapsed time is " + elapsed);
        }
      }, e => {
        if (e instanceof HttpErrorResponse && e.status === 400) {
          this.snackBar.open("Cannot fetch data, something went wrong", 'Close', {
            duration: 3000
          });
        }
      }
    ));
  }
}
