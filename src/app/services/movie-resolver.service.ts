import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Movie} from "../model/Movie";
import {Observable} from "rxjs";
import {MovieService} from "./movie.service";
import {resolve} from "@angular/compiler-cli/src/ngtsc/file_system";

@Injectable()
export class MovieResolverService implements Resolve<Movie> {

  constructor(private movieService: MovieService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Movie> | Promise<Movie> | Movie {
    return  this.movieService.getMovie(Number(route.paramMap.get("id")));

    // return new Promise(resolve1 => {
    //     setTimeout(() => {
    //       console.log("resolve movie")
    //        resolve1( movie);
    //     }, 10000)
    //   }
    // )
  }
}
