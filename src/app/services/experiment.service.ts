import {Injectable} from '@angular/core';
import {interval, Observable, of} from "rxjs";
import {debounceTime, map, switchMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ExperimentService {
  private static count: number = 0;

  constructor() {
  }

  test() {
    ExperimentService.count += 1;
    return new Observable(subscriber => {
      setInterval(() => {
        subscriber.next("subscribed count= " + ExperimentService.count + " Today=  " + new Date());
      }, 2000)
      // setTimeout(() => {
      //   subscriber.error("Error !!! ");
      // }, 4000)
      setTimeout(() => {
        subscriber.complete();
      }, 20000)
    });
  }

  simulateHttp(val: any, delay: number) {//short-lived stream
    return of(val).pipe(debounceTime(delay));
  }

  private simulateFirebase(val: any, delay: number) { //Long-lived stream
    return interval(delay).pipe(map(index => val + " " + index));
  }

  switchMapDemo() {
    let observable1$ = this.simulateFirebase("customer 1 ", 5000);
    return observable1$.pipe(switchMap(sourceValue => {
      console.log(sourceValue)
      return this.simulateFirebase("inner observable ", 1000)
    })).subscribe(value => console.log(value));
  }

  switchMapDemo2() {
    const course$ = this.simulateHttp({id: 1, description: 'Angular For Beginners'}, 1000);

    return course$.pipe(
      switchMap(courses => this.simulateHttp([], 2000)
        .pipe(
          map(lessons => [courses, lessons])
        ),
      )).subscribe(value => console.log(value));
  }
}
