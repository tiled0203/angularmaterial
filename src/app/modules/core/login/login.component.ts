import {Component, OnInit} from '@angular/core';
import {LoginService} from "../../../services/login.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private return: string = "";

  constructor(private loginService: LoginService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => this.return = params['return'] || '/home');
  }

  login(loginFormValues: any) {
    if (loginFormValues.password && loginFormValues.username) {
      this.loginService.login(loginFormValues);
      this.router.navigateByUrl(this.return);
    }
  }


}
