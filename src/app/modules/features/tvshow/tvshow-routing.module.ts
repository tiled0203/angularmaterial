import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TvShowAddComponent} from "./tv-show-add/tv-show-add.component";
import {TvShowDetailComponent} from "./tv-show-detail/tv-show-detail.component";
import {AuthGuard} from "../../../services/auth.guard";
import {TvShowManualAddComponent} from "./tv-show-manual-add/tv-show-manual-add.component";

const routes: Routes = [
  {path: 'add', component: TvShowAddComponent, canActivate: [AuthGuard]},
  {path: 'detail/:id', component: TvShowDetailComponent},
  {path: 'new', component: TvShowManualAddComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TvshowRoutingModule { }
