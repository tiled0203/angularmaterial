import {Component, OnInit} from '@angular/core';
import {Movie} from "../../../../model/Movie";
import {MovieService} from "../../../../services/movie.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TvShowService} from "../../../../services/tv-show.service";
import {TvShow} from "../../../../model/TvShow";
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "../../../shared/dialog/dialog.component";

@Component({
  selector: 'app-tv-show-detail',
  templateUrl: './tv-show-detail.component.html',
  styleUrls: ['./tv-show-detail.component.css']
})
export class TvShowDetailComponent implements OnInit {

  foundTvShow: TvShow | undefined;

  constructor(private tvShowService: TvShowService, private activateRoute: ActivatedRoute, private router: Router, private dialog: MatDialog) {

  }

  ngOnInit(): void {
    let id = this.activateRoute.snapshot.paramMap.get('id');
    this.tvShowService.getTvShow(Number(id)).then(tvShowReponse => this.foundTvShow = tvShowReponse);
  }

  openDialog(tvShow: TvShow | undefined): void {
    if (tvShow) {
      const dialogRef = this.dialog.open(DialogComponent, {
        width: '250px',
        data: {title: tvShow.title}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log('Movie has been added');
          this.router.navigate(['tvshow/home']);
        } else {
          this.tvShowService.delete(tvShow.id).then();
        }
      });
    }
  }
}
