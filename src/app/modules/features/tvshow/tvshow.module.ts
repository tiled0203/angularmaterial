import {NgModule} from '@angular/core';
import {TvShowAddComponent} from "./tv-show-add/tv-show-add.component";
import {TvShowDetailComponent} from "./tv-show-detail/tv-show-detail.component";
import {TvShowService} from "../../../services/tv-show.service";
import {SharedModule} from "../../shared/shared.module";
import {TvShowHomeComponent} from "../pages/tv-show-home/tv-show-home.component";
import {TvshowRoutingModule} from "./tvshow-routing.module";
import { TvShowManualAddComponent } from './tv-show-manual-add/tv-show-manual-add.component';
import {CommonModule} from "@angular/common";


@NgModule({
  declarations: [TvShowAddComponent, TvShowDetailComponent, TvShowHomeComponent, TvShowManualAddComponent],
    imports: [SharedModule, TvshowRoutingModule],
  providers: [TvShowService]
})
export class TvshowModule {
}
