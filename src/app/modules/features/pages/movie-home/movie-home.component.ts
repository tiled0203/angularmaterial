import {Component, OnInit} from '@angular/core';
import {Movie} from "../../../../model/Movie";
import {MovieService} from "../../../../services/movie.service";
import {Router} from "@angular/router";
import {TvShow} from "../../../../model/TvShow";
import {Item} from "../../../../model/Item";
import {switchMap, tap} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-movie-home',
  templateUrl: './movie-home.component.html',
  styleUrls: ['./movie-home.component.css']
})
export class MovieHomeComponent implements OnInit {
  movieList: Movie[] = [];
  isOnline = false;
  private title: string = "";

  constructor(private movieService: MovieService, private router: Router) {
  }

  ngOnInit(): void {
    this.fetchMovies();
  }

  private fetchMovies() {
    this.movieService.getMovies().subscribe(movieReponse => this.movieList = movieReponse);
  }

  navigateToDetail(movie: Movie) {
    this.router.navigate(['movie/detail', movie.id]);
  }

  deleteMovie(movie: Item) {
    this.movieService.delete(movie.id).then(() => this.fetchMovies());
  }

  searchTitle(title: string) {
    this.title = title;
    this.movieService.lookupMovie(title, this.isOnline).subscribe(value => this.movieList = value, error => this.movieList = []);
  }

  toggleOnline() {
    this.isOnline = !this.isOnline;
    if (this.isOnline && this.title === "") {
      this.movieList = [];
    } else if(this.title !== "") {
      this.searchTitle(this.title);
    }else{
      this.fetchMovies();
    }
  }
}
