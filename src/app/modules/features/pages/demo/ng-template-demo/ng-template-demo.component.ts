import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ng-template-demo',
  templateUrl: './ng-template-demo.component.html',
  styleUrls: ['./ng-template-demo.component.css']
})
export class NgTemplateDemoComponent implements OnInit {
  context = {
    myFirstName: 'John Doe',
    greeting(name: string) {
      alert(`Hello! ${name}`);
    }
  };

  constructor() {
  }

  ngOnInit(): void {
  }

}
