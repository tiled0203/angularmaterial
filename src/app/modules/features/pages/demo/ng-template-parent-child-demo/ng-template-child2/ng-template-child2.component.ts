import {Component, ElementRef, Input, OnInit, TemplateRef} from '@angular/core';

@Component({
  selector: 'app-ng-template-child2',
  templateUrl: './ng-template-child2.component.html',
  styleUrls: ['./ng-template-child2.component.css']
})
export class NgTemplateChild2Component implements OnInit {
  @Input() someTemplate: TemplateRef<ElementRef> | undefined  ;
  context = {
    title: 'Second component',
    someMethod(title: string) {
      const question = prompt(`Question for ${title}: How old are you?`, '18');
      console.log(question);
    }
  };

  constructor() {
  }

  ngOnInit(): void {
  }

}
