import {Component, ElementRef, Input, OnInit, TemplateRef} from '@angular/core';

@Component({
  selector: 'app-ng-template-child1',
  templateUrl: './ng-template-child1.component.html',
  styleUrls: ['./ng-template-child1.component.css']
})
export class NgTemplateChild1Component implements OnInit {
  @Input() someTemplate: TemplateRef<ElementRef> | undefined  ;
  context = {
    title: 'First component',
    someMethod(title: string) {
      alert(`Simple alert for ${title}`);
    }
  };

  constructor() { }

  ngOnInit(): void {
  }

}
