import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-view-child-child-demo',
  templateUrl: './view-child-child-demo.component.html',
  styleUrls: ['./view-child-child-demo.component.css']
})
export class ViewChildChildDemoComponent {
  message: string = '';
  count: number = 0;

  increaseByOne() {
    this.count = this.count + 1;
    this.message = "Counter: " + this.count;
  }

  decreaseByOne() {
    this.count = this.count - 1;
    this.message = "Counter: " + this.count;
  }

}
