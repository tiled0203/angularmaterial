import {Component, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {CpColorDirective} from "../cp-color.directive";

@Component({
  selector: 'app-cpcolor-parent',
  templateUrl: './cp-color-parent.component.html',
  styleUrls: ['./cp-color-parent.component.css']
})
export class CpColorParentComponent {

  @ViewChildren(CpColorDirective)
  private cpColorDirective = [] as CpColorDirective[];

  changeColor(color: string) {
    this.cpColorDirective.forEach(value => value.change(color));
  }
}
