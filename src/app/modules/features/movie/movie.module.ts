import {NgModule} from '@angular/core';
import {SharedModule} from "../../shared/shared.module";
import {MovieAddComponent} from "./movie-add/movie-add.component";
import {MovieDetailComponent} from "./movie-detail/movie-detail.component";
import {MovieService} from "../../../services/movie.service";
import {MovieHomeComponent} from "../pages/movie-home/movie-home.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MovieRoutingModule} from "./movie-routing.module";
import {CoreModule} from "../../core/core.module";
import {MovieResolverService} from "../../../services/movie-resolver.service";
import {MovieStoreService} from "../../../services/movie-store.service";


@NgModule({
  declarations: [MovieAddComponent, MovieDetailComponent, MovieHomeComponent],
  imports: [
    SharedModule, MovieRoutingModule
  ], providers: [MovieStoreService,MovieService, MovieResolverService]
})
export class MovieModule {
}

