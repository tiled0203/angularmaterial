import {Component, OnInit} from '@angular/core';
import {MovieService} from "../../../../services/movie.service";
import {Movie} from "../../../../model/Movie";
import {ActivatedRoute, Router} from "@angular/router";
import {TvShow} from "../../../../model/TvShow";
import {DialogComponent} from "../../../shared/dialog/dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {
  foundMovie: Movie | undefined;

  constructor(private movieService: MovieService, private activateRoute: ActivatedRoute, private router: Router, private dialog: MatDialog) {

  }

  ngOnInit(): void {
    // let id = this.activateRoute.snapshot.paramMap.get('id');
    // this.movieService.getMovie(Number(id)).then(movieReponse => this.foundMovie = movieReponse);
    this.foundMovie = this.activateRoute.snapshot.data['movie'];
  }


  openDialog(foundMovie: Movie | undefined): void {
    if (foundMovie) {
      const dialogRef = this.dialog.open(DialogComponent, {
        width: '250px',
        data: {title: foundMovie.title}
      });

      dialogRef.afterClosed().toPromise().then(result => {
        if (result) {
          console.log('Movie has been added');
          this.router.navigate(['movie/list']);
        } else {
          this.movieService.delete(foundMovie.id).then();
        }
      });
    }
  }
}
