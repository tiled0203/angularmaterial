import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MovieAddComponent} from "./movie-add/movie-add.component";
import {MovieDetailComponent} from "./movie-detail/movie-detail.component";
import {AuthGuard} from "../../../services/auth.guard";
import {MovieResolverService} from "../../../services/movie-resolver.service";

const routes: Routes = [
  {path: 'add', component: MovieAddComponent, canActivate: [AuthGuard]},
  {
    path: 'detail/:id', component: MovieDetailComponent, resolve: {
      movie: MovieResolverService
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRoutingModule {
}
