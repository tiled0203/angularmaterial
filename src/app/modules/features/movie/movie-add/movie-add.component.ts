import {Component, OnInit} from '@angular/core';
import {Movie} from "../../../../model/Movie";
import {MovieService} from "../../../../services/movie.service";
import {Router} from "@angular/router";
import {MovieStoreService} from "../../../../services/movie-store.service";
import {MovieSearchHistory} from "../../../../model/MovieSearchHistory";
import {Location} from "@angular/common";

@Component({
  selector: 'app-movie-add',
  templateUrl: './movie-add.component.html',
  styleUrls: ['./movie-add.component.css']
})
export class MovieAddComponent implements OnInit {
  title: string | undefined;
  movies: Movie[] = [];


  constructor(private movieService: MovieService, private location: Location, private router: Router, private movieStoreService: MovieStoreService) {

  }

  ngOnInit(): void {
    console.log(history.state)
    if (!history.state.resetHistory) {
      this.movies = this.movieStoreService.getMovies();
      this.title = this.movieStoreService.searchTitle;
      console.log(this.movies, this.title)
    }
  }

  search(searchedMovie: string) {
    // console.log(ngForm.form.controls.title.errors);
    // if (ngForm.valid) {
    this.movieService.lookupMovie(searchedMovie, true).subscribe(movieResponse => {
      this.movies = movieResponse
      this.movieStoreService.setFoundMovies(new MovieSearchHistory(searchedMovie, movieResponse));
    });
    // }
  }

  addMovieToCollectionAndGotToDetail(movie: Movie) {
    this.movieService.addMovie(movie.onlineId).then(movieResponse => {
      this.router.navigate(['movie/detail', movieResponse.id]);
    });
  }

}
