import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Host,
  Inject,
  InjectionToken,
  Input,
  Optional,
  ViewContainerRef
} from '@angular/core';
import {NgControl} from '@angular/forms';
import {ControlErrorComponent} from '../control-error/control-error.component';
import {FormSubmitDirective} from './form-submit.directive';
import {EMPTY, merge, Observable} from "rxjs";
import {MovieService} from "../../../services/movie.service";


export const defaultErrors = {
  required: (error: any) => `This field is required`,
  forbiddenTitle: (error: any) => `This input is forbidden`,
// @ts-ignore
  minlength: ({requiredLength, actualLength}) => `Expect ${requiredLength} but got ${actualLength}`,
  // @ts-ignore
  max: ({max, actual}) => `Expect a number less than ${max} but got ${actual}`,
  // @ts-ignore
  min: ({min, actual}) => `Expect a number lager than ${min} but got ${actual}`
}

export const FORM_ERRORS = new InjectionToken('FORM_ERRORS', {
  factory: () => defaultErrors
});


@Directive({
  selector: '[formControl], [formControlName], [ngModel]'
})
export class ControlErrorsDirective {
  private ref: ComponentRef<ControlErrorComponent> | undefined;
  private submit$: Observable<Event>;
  @Input() customErrors: any = {};

  static count: number = 0;

  constructor(
    private movieService: MovieService,
    private vcr: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
    @Inject(FORM_ERRORS) private errors: any,
    @Optional() @Host() private form: FormSubmitDirective,
    private controlDir: NgControl) {
    this.submit$ = this.form ? this.form.submit$ : EMPTY;
    ControlErrorsDirective.count += 1;
    console.log(ControlErrorsDirective.count)
  }

  ngOnInit() {
    // @ts-ignore
    this.controlDir.valueChanges
      .pipe(inputValue$ => merge(inputValue$, this.submit$)).subscribe(() => {
      const controlErrors = this.controlDir.errors;
      if (controlErrors) {
        const firstKey = Object.keys(controlErrors)[0];
        const getError = this.errors[firstKey];
        console.log(getError, controlErrors)
        const text = this.customErrors[firstKey] || getError(controlErrors[firstKey]);
        this.setError(text);
      } else if (this.ref) {
        this.setError("");
      }
    })
  }

  setError(text: string) {
    if (!this.ref) {
      const factory = this.resolver.resolveComponentFactory(ControlErrorComponent);
      // @ts-ignore
      this.ref = this.vcr.createComponent(factory);
    }

    this.ref.instance.text = text;
  }

  ngOnDestroy() {
  }

}
