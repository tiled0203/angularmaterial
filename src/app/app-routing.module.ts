import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MovieHomeComponent} from "./modules/features/pages/movie-home/movie-home.component";
import {TvShowHomeComponent} from "./modules/features/pages/tv-show-home/tv-show-home.component";
import {HomeComponent} from "./modules/core/home/home.component";
import {ExperimentComponent} from "./modules/features/pages/experiment/experiment.component";
import {LoginComponent} from "./modules/core/login/login.component";
import {CustomPreloadService} from "./services/custom-preload.service";
import {NgTemplateDemoComponent} from "./modules/features/pages/demo/ng-template-demo/ng-template-demo.component";
import {
  NgTemplateParentComponent
} from "./modules/features/pages/demo/ng-template-parent-child-demo/ng-template-parent/ng-template-parent.component";
import {
  ViewChildParentDemoComponent
} from "./modules/features/pages/demo/view-child/view-child-parent-demo/view-child-parent-demo.component";
import {
  CpColorParentComponent
} from "./modules/features/pages/demo/view-child/view-child-using-directive/cpcolor-parent/cp-color-parent.component";

export const routes: Routes = [
  {path: "home", component: HomeComponent},
  {path: "login", component: LoginComponent},
  {path: 'tvshow/home', component: TvShowHomeComponent},
  {
    path: 'tvshow',
    loadChildren: () => import("./modules/features/tvshow/tvshow.module").then(value => value.TvshowModule)
  },
  {path: 'movie/home', component: MovieHomeComponent},
  {
    path: 'movie',
    loadChildren: () => import("./modules/features/movie/movie.module").then(value => value.MovieModule),
    data: {preload: true}
  },
  {path: 'experiment', component: ExperimentComponent},
  {path: 'demo/ngtemplate', component: NgTemplateDemoComponent},
  {path: 'demo/ngtemplateparentchild', component: NgTemplateParentComponent},
  {path: 'demo/viewchild-parent-child', component: ViewChildParentDemoComponent},
  {path: 'demo/viewchild-directive', component: CpColorParentComponent},
  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: CustomPreloadService})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
