import {Movie} from "./Movie";

export class MovieSearchHistory {
  title: string = '';
  movies: Movie[] = [];

  constructor(title: string, movies: Movie[]) {
    this.title = title;
    this.movies = movies;
  }
}
