import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from '@angular/common/http';
import {CoreModule} from "./modules/core/core.module";
import {ExperimentComponent} from './modules/features/pages/experiment/experiment.component';
import {NgTemplateDemoComponent} from "./modules/features/pages/demo/ng-template-demo/ng-template-demo.component";
import {SharedModule} from "./modules/shared/shared.module";
import {
  NgTemplateParentComponent
} from './modules/features/pages/demo/ng-template-parent-child-demo/ng-template-parent/ng-template-parent.component';
import {
  NgTemplateChild2Component
} from './modules/features/pages/demo/ng-template-parent-child-demo/ng-template-child2/ng-template-child2.component';
import {
  NgTemplateChild1Component
} from "./modules/features/pages/demo/ng-template-parent-child-demo/ng-template-child1/ng-template-child1.component";
import {
  ViewChildChildDemoComponent
} from './modules/features/pages/demo/view-child/view-child-child-demo/view-child-child-demo.component';
import {CpColorDirective} from './modules/features/pages/demo/view-child/view-child-using-directive/cp-color.directive';
import {
  CpColorParentComponent
} from './modules/features/pages/demo/view-child/view-child-using-directive/cpcolor-parent/cp-color-parent.component';
import {
  ViewChildParentDemoComponent
} from "./modules/features/pages/demo/view-child/view-child-parent-demo/view-child-parent-demo.component";

@NgModule({
  declarations: [
    AppComponent,
    ExperimentComponent,
    NgTemplateDemoComponent,
    NgTemplateParentComponent,
    NgTemplateChild1Component,
    NgTemplateChild2Component,
    ViewChildParentDemoComponent,
    ViewChildChildDemoComponent,
    CpColorDirective,
    CpColorParentComponent,
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [],
  exports:[],
  bootstrap: [AppComponent]
})
export class AppModule {
}
